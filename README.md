# DISCLAIMER: Due to GitLab only allowing the repository to be viewed under the Maintainer's account and not the Developers' account this repository has been cloned and reuploaded to show up under all Developers' repository list.
# Bengali Grammar Checker
*NSU Capstone Project* 
## A Linux based Semantic Grammar Parser.
### Tokenizer

**Installation**
```bash
$ pip install git+git://github.com/irshadbhat/indic-tokenizer.git
```
**Execution**
```shell
$ ind-tokz --i inputFile.txt --o outputFile.txt --l ben
```
### Tagger

**Installation**
- Install the tagger
```bash
$ sudo python setup.py
```
- Install NLTK 2.0.4
- Download NLTK *Indian* corpora
```python
import nltk
nltk.download('indian')
```

**Execution**
Example file has been given named *run.py* . Put the tokenized string in *input.txt* and run the script-
```bash
python run.py
```
### Mapper

**Execution**
- Name the output file of POS tagger 'pos_tagged.txt' (*you can change the name in the source or make it an execution parameter*)
- Then run the python script

```bash
$ python mapper.py
```
**Developers' names-**
1. Md. Asimuzzaman Mansib (@AMansib)
2. Farah Hossain (@Farahowo)
