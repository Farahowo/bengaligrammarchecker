## Installation
- Install the tagger
```bash
$ sudo python setup.py
```
- Install NLTK 2.0.4
- Download NLTK *Indian* corpora
```python
import nltk
nltk.download('indian')
```

## Execution
Example file has been given named *run.py* . Put the tokenized string in *input.txt* and run the script-
```bash
python run.py
```

