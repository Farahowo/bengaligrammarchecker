# -*- coding: utf-8 -*- 
import bangla_pos_tagger

btagger = bangla_pos_tagger.BanglaTagger()

words = []

with open('input.txt') as file:

	words = file.read().splitlines()

tagged = btagger.pos_tag(words)

length = len(tagged)
output = []
outfile = open('pos_tagged.txt','w')

for num in range(0,length):
	temp = tagged[num][0]
	out = temp + ", " + tagged[num][1]
	outfile.write("%s\n" % out)
	#print str + " " + tagged[num][1]

outfile.close()
#print words
