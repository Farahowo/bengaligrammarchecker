# -*- coding: utf-8 -*- 
import bangla_pos_tagger

btagger = bangla_pos_tagger.BanglaTagger()

words = []

with open('input.txt') as file:
	#words = file.read().splitlines()
	input = file.read()
	words = input.split()

tagged = btagger.pos_tag(words)

length = len(tagged)
#output = []
final = ""

outfile = open('pos_mapped.txt','w')

for num in range(0,length):
	temp = tagged[num][0]
	
	#out = temp + ", " + tagged[num][1]
	out = temp + ","
	#outfile.write("%s\n" % out)
	#print str + " " + tagged[num][1]
	pos = tagged[num][1]
	
	if pos in ("JJ", "JQ", "DAB", "DRL", "DWH"):
		#output.append("adjective")
		out = out + "adjective"
	elif pos in ("NC","NP","NV","NST","NN"):
		#output.append("noun")
		out = out + "noun"
	elif pos in ("PPR", "PRF", "PRC", "PRL", "PWH","PRP"):
		#output.append("pronoun")
		out = out + "pronoun"
	elif pos in ("AMN", "ALC", "LRL", "LV", "LN", "LC"):
		#output.append("adverb")
		out = out + "adverb"
	elif pos =="PU":
		#output.append("xp")
		out = out + "xp"
	elif pos in ("VM", "VA","VAUX"):
		#output.append("verb")
		out = out + "verb"
	elif pos == "PP":
		#output.append("adpos")
		out = out + "adpos"
	elif pos == "CX":
		#output.append("particle")
		out = out + "particle"
	
	final = final + out + " "

outfile.write("%s\n" % final)
outfile.close()