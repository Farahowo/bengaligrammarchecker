## Installation
```bash
$ pip install git+git://github.com/irshadbhat/indic-tokenizer.git
```
## Execution
```shell
$ ind-tokz --i inputFile.txt --o outputFile.txt --l ben
```