## Execution
- Name the output file of POS tagger 'pos_tagged.txt' (*you can change the name in the source or make it an execution parameter*)
- Then run the python script

```bash
$ python mapper.py
```