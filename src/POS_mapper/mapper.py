with open('pos_tagged.txt') as file:
        #words = file.readlines()
        words = file.read().splitlines()

#print words
length = len(words)

for x in xrange(0,length):

	token_set = words[x].split(",",1)

	pos = token_set[1][1:]

	#keeping both of the previous pos tag and mapped pos tag with value
	#output = [token_set[0],pos]

	#keeping the mappes pos tag only with value
	output = [token_set[0]]

	if pos in ("JJ", "JQ", "DAB", "DRL", "DWH"):
		output.append("adjective")
	elif pos in ("NC","NP","NV","NST","NN"):
		output.append("noun")
	elif pos in ("PPR", "PRF", "PRC", "PRL", "PWH"):
		output.append("pronoun")
	elif pos in ("AMN", "ALC", "LRL", "LV", "LN", "LC"):
		output.append("adverb")
	elif pos =="PU":
		output.append("xp")
	elif pos in ("VM", "VA","VAUX"):
		output.append("verb")
	elif pos == "PP":
		output.append("adpos") #subject to change
	elif pos == "CX"
		output.append("particle") #subject to change

	print output